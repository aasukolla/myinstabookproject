    package com.InstaBook;
     /**
      * This is an implementation of a class login menu
      * 
      * @author aasukolla
      *
      */
    public class LoginMenu 
      {

	      String   username;     //it represents the username of a login menu          
	      String   password;    //it represents the password of a login menu
	      String   fg_passw;    //it represents the forgot password a login menu  
	
	  //Initializing the parametarized constructor
	      
	public LoginMenu(String username,String Password,String fg_passw) 
	   {
		  //Declare the instance variable
		
		  this.username=username;
		  this.password=Password;
		  this.fg_passw=fg_passw;
	   }


	//Initializing the instance method
	 public void logic()
	    {  
		    System.out.println("The login menu page detaials are shown below");
		    System.out.println("==================================================");
		    System.out.println("The username is:"+username);
		    System.out.println("The password is:"+password);
		    System.out.println("The forgot password is:"+fg_passw);
		}
	 
	 //Initializing the another instance method
	  public void logic1()throws Exception
	    {
		  if(username.trim().length()<2 || username.trim().length()>15)
			 {
			  //validation of name failure
			  throw new Exception("username is too short");
			 }
		     
		  else if(password.trim().length()<2 || password.trim().length()>8)
		    {
			  //validation of password failure
			  throw new Exception("password is too short");
		    }
		  else
		  {
			  System.out.println("username and password is worng,we cannot login");
		  }
	    }
 }
